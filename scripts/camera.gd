extends Camera2D

var touches = []

func _input(event):
	if event is InputEventScreenDrag:
		if touches.empty() or touches[0].index != event.index:
			touches.append(event)
		if len(touches) == 2:
			var distance = touches[0].position.distance_to(touches[1].position)
			var prev_distance = (touches[0].position - touches[0].relative).distance_to(touches[1].position - touches[1].relative)
			change_zoom((prev_distance - distance) / 300)
#			position -= (touches[0].relative + touches[1].relative) / zoom
			touches.clear()
	change_zoom((float(event.is_action_pressed('zoom_out')) - float(event.is_action_pressed('zoom_in'))) / 10)

func change_zoom(delta):
	zoom = Vector2.ONE * clamp(zoom.x + delta, 1, 10)
