extends StaticBody2D
tool

export var noise:OpenSimplexNoise
export var _scale = Vector3(0.001, 1000, 10)
export var resolution:int = 200
export(int, 0, 100) var _layers = 1
export(float) var y_gradient = 0
export(Color) var core_color
export(Color) var surface_color
export(bool) var regenerate setget set_regenerate

var terrain_layers:Dictionary
var collision:CollisionPolygon2D

func parallax_to_world(value, center, distance):
	return (value - center) / (2/(1-distance)-1) + center

func world_to_parallax(value, center, distance):
	return (value - center) * (2/(1-distance)-1) + center

func set_regenerate(value):
	_ready()

func _ready():
	collision = get_node('CollisionPolygon2D')
	for c in terrain_layers.values():
		c.free()
	terrain_layers.clear()
	var scene = preload('res://scenes/terrain_layer.tscn')
	for l in _layers:
		terrain_layers[+l / float(_layers)] = scene.instance()
		terrain_layers[-l / float(_layers)] = scene.instance()
	for d in terrain_layers:
		var c = terrain_layers[d]
		c.z_index = -d*_layers
		c.position.y = (2/(1+d)-2)*y_gradient
		if d > 0:
			c.core_color = core_color.lightened(d/2)
			c.surface_color = surface_color.lightened(d/2)
			c.get_node('Surface').hide()
		elif d < 0:
			c.core_color = core_color.darkened(-d/2)
			c.surface_color = surface_color.darkened(-d/2)
			c.get_node('Surface').hide()
		else:
			c.core_color = core_color
			c.surface_color = surface_color
		c.get_node('Transition').width = 1000 * (1 - d)
		c.get_node('Core').offset.y = 1000 * (1 - d) / 3
		add_child(c)
		c.set_owner(get_tree().edited_scene_root)

func _physics_process(delta):
	var inverse_viewport_transform = get_viewport_transform().affine_inverse()
	var vp_start = inverse_viewport_transform * Vector2.ZERO - global_position
	var vp_end = inverse_viewport_transform * get_viewport().size - global_position
	var vp_center = (vp_start + vp_end) / 2
	
	var x_min = floor(vp_start.x / resolution) * resolution
	var x_max = ceil(vp_end.x / resolution) * resolution
	
	for d in terrain_layers:
		var c = terrain_layers[d]
		var points = PoolVector2Array()
		var rp = resolution / (1 - d)
		var xp_min = floor(world_to_parallax(vp_start.x, vp_center.x, d) / rp) * rp
		var xp_max = ceil(world_to_parallax(vp_end.x, vp_center.x, d) / rp) * rp
		for xp in range(xp_min, xp_max+rp+1, rp):
			var yp = noise.get_noise_2d(xp*_scale.x, d*_scale.z)*_scale.y
			points.append(parallax_to_world(Vector2(xp, yp), vp_center, d))
		c.points = points
	
	collision.polygon = terrain_layers[float(0)].get_node('Core').polygon
