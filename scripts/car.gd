extends Node2D

func save_to_file():
	var file = File.new()
	file.open('user://car.json', File.WRITE)
	var descriptors = []
	for c in get_children():
		descriptors.append(c.descriptor)
	file.store_string(to_json(descriptors))

func restore_from_file():
	var file = File.new()
	if file.open('user://car.json', File.READ) != OK:
		print('unable to load car.json')
		return
	var descriptors = parse_vectors(parse_json(file.get_as_text()))
	if descriptors == null:
		return
	for c in get_children():
		if 'descriptor' in c:
			c.queue_free()
	for d in descriptors:
		var c = load('res://scenes/' + d.type + '.tscn').instance()
		add_child(c)
		c.descriptor = d
		c.descriptor_update()

func build():
	for c in get_children():
		if c is Beam or c is Motor:
			c.build()
	for c in get_children():
		if c is PivotBeam:
			c.build()
	for c in get_children():
		if c is Suspension or c is DriveShaft:
			c.build()
	for c in get_children():
		if c is Wheel:
			c.build()

func parse_vectors(value):
	if value is Dictionary:
		for k in value.keys():
			value[k] = parse_vectors(value[k])
	elif value is Array:
		for i in len(value):
			value[i] = parse_vectors(value[i])
	elif value is String:
		if value[0] == '(' and value[-1] == ')':
			var components:Array = value.substr(1, value.length()-2).split(',')
			value = Vector2(components[0], components[1])
	return value
