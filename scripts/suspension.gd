extends Node2D
class_name Suspension

export(bool) var fixed

onready var spring = $DampedSpringJoint2D
onready var groove = $GrooveJoint2D
onready var main_sprite = $MainSprite
onready var top_sprite = $TopSprite
onready var bottom_sprite = $BottomSprite
onready var target = $SuspensionTarget
var descriptor:Dictionary

func _process(delta):
	var position_delta = target.global_position - global_position
	main_sprite.scale.y = position_delta.length()
	main_sprite.global_rotation = Vector2.DOWN.angle_to(position_delta)
	bottom_sprite.global_position = target.global_position

func descriptor_update():
	var cell_size = Grid.get_cell_size()
	position = descriptor.a * cell_size
	target.global_position = get_parent().to_global(descriptor.b * cell_size)

func build():
	var car = get_parent()
	for c in car.get_children():
		if c is RigidBody2D and c.has_point(global_position):
			var g = global_position
			var tg = target.global_position
			car.remove_child(self)
			c.add_child(self)
			global_position = g
			target.global_position = tg
			spring.node_a = spring.get_path_to(c)
			if fixed:
				groove.node_a = groove.get_path_to(c)
			break
	for c in car.get_children():
		if c is RigidBody2D and c.has_point(target.global_position):
			var g = target.global_position
			remove_child(target)
			c.add_child(target)
			target.global_position = g
			var position_delta = target.global_position - global_position
			global_rotation = Vector2.DOWN.angle_to(position_delta)
			spring.length = position_delta.length()
			spring.node_b = spring.get_path_to(c)
			if fixed:
				groove.length = position_delta.length()
				groove.initial_offset = position_delta.length()
				groove.node_b = groove.get_path_to(c)
			break

func has_point(point:Vector2) -> bool:
	return main_sprite.get_rect().has_point(main_sprite.to_local(point)) \
		or top_sprite.get_rect().has_point(top_sprite.to_local(point)) \
		or bottom_sprite.get_rect().has_point(bottom_sprite.to_local(point))

func on_editor_touch(cell:Vector2):
	descriptor = {
		'type': ('fixed' if fixed else 'pivot') + '_suspension',
		'a': cell,
		'b': cell,
	}
	descriptor_update()

func on_editor_drag(cell:Vector2):
	descriptor.b = cell
	descriptor_update()

func on_editor_release(cell:Vector2):
	pass
