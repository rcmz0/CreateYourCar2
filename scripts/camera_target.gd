extends Node2D

onready var camera:Camera2D = get_child(0)

func _physics_process(delta):
	var tracked_nodes = []
	for c in get_parent().get_children():
		if c is RigidBody2D:
			tracked_nodes.append(c)
	var center_of_gravity = Vector2.ZERO
	for c in tracked_nodes:
		center_of_gravity += c.global_position
	center_of_gravity /= len(tracked_nodes)
	global_position = center_of_gravity
	camera.align()
