extends Button

export(String) var action

func _ready():
	if not toggle_mode:
		connect('button_down', self, '_toggled', [true])
		connect('button_up', self, '_toggled', [false])

func _toggled(pressed):
	var ev = InputEventAction.new()
	ev.action = action
	ev.pressed = pressed
	Input.parse_input_event(ev)
