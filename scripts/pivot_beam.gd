extends CollisionShape2D
class_name PivotBeam

onready var main_line = $MainLine2D
onready var detail_line = $DetailLine2D
onready var sprite = $Sprite
onready var pin = $PinJoint2D
var descriptor:Dictionary

func get_mass():
	return descriptor.a.distance_to(descriptor.b) * 0.05

func descriptor_update():
	var cell_size = Grid.get_cell_size()
	
	rotation = Vector2.DOWN.angle_to(descriptor.a - descriptor.b)
	var length = descriptor.a.distance_to(descriptor.b) * cell_size
	var center = (descriptor.a + descriptor.b) / 2 * cell_size
	position = center
	global_position = to_global(Vector2(0, -shape.radius-1))
	shape.height = length - shape.radius*2
	
	if length == 0: length = 0.001
	main_line.points[0].y = -length/2
	main_line.points[1].y = +length/2
	detail_line.points[0].y = -length/2
	detail_line.points[1].y = +length/2
	
	sprite.position.y = length/2 + shape.radius
	pin.position.y = length/2 + shape.radius

func build():
	load('res://scenes/frame.tscn').instance().build(self)
	pin.node_a = pin.get_path_to(get_parent())
	for c in get_parent().get_parent().get_children():
		if c.get_script().resource_path.get_file() == 'frame.gd' and c.has_point(pin.global_position):
			pin.node_b = pin.get_path_to(c)
			break

func has_point(point:Vector2) -> bool:
	var r = -main_line.width/2
	var h = abs(main_line.points[0].y - main_line.points[1].y)
	return Rect2(
			Vector2(-shape.radius, -shape.height/2-shape.radius),
			Vector2(shape.radius*2, shape.height+shape.radius*2)
		).has_point(to_local(point))

func on_editor_touch(cell:Vector2):
	descriptor = {
		'type': 'pivot_beam',
		'a': cell,
		'b': cell,
	}
	descriptor_update()

func on_editor_drag(cell:Vector2):
	descriptor.b = cell
	descriptor_update()
	
func on_editor_release(cell:Vector2):
	pass
