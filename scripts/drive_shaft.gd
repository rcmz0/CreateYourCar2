extends Node2D
class_name DriveShaft

onready var shaft = $Shaft
onready var target = $DriveShaftTarget
onready var sprites = [$Shaft, $FrontGear, $SideGear, $DriveShaftTarget/FrontGear, $DriveShaftTarget/SideGear]
var motor
var wheel
var descriptor:Dictionary

func _physics_process(delta):
	if motor != null and wheel != null:
		wheel.apply_torque_impulse(wheel.mass * motor.direction * 5_000)
	
func _process(delta):
	shaft.scale.x = global_position.distance_to(target.global_position)
	var tg = target.global_position
	global_rotation = Vector2.RIGHT.angle_to(target.global_position - global_position)
	target.global_position = tg
	target.global_rotation = global_rotation
	
	var angle = -wheel.global_rotation*PI*2 if wheel else OS.get_ticks_msec()/30.0
	for g in sprites:
		g.material.set_shader_param('angle', angle)

func descriptor_update():
	var cell_size = Grid.get_cell_size()
	position = descriptor.a * cell_size
	target.global_position = get_parent().to_global(descriptor.b * cell_size)

func build():
	var car = get_parent()
	for c in car.get_children():
		if c is RigidBody2D and c.has_point(global_position):
			var g = global_position
			var tg = target.global_position
			car.remove_child(self)
			c.add_child(self)
			global_position = g
			target.global_position = tg
			break
	for c in car.get_children():
		if c is RigidBody2D and c.has_point(target.global_position):
			var g = target.global_position
			remove_child(target)
			c.add_child(target)
			target.global_position = g
			break
	
	for c in get_parent().get_children():
		if c is Motor and c.has_point(global_position):
			motor = c
			break
	if target.get_parent() is Wheel \
		and target.get_parent().global_position == target.global_position:
			wheel = target.get_parent()

func has_point(point:Vector2):
	for s in sprites:
		if s.get_rect().has_point(s.to_local(point)):
			return true
	return false

func on_editor_touch(cell:Vector2):
	descriptor = {
		'type': 'drive_shaft',
		'a': cell,
		'b': cell,
	}
	descriptor_update()

func on_editor_drag(cell:Vector2):
	descriptor.b = cell
	descriptor_update()

func on_editor_release(cell:Vector2):
	pass
