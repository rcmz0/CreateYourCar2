extends Button

func _get_minimum_size():
	var s = rect_size
	var m = max(s.x, s.y)
	return Vector2.ONE*m
