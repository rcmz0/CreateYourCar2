extends Control
class_name Grid
tool

export(int) var line_thickness = 3

onready var car = get_node('Car')
onready var bottom_bar:Control = get_node('../../MarginContainer/Control/BottomBar')
onready var remove_button:Button = get_node('../../MarginContainer/Control/BottomBar/Remove')

var cell_size = get_cell_size()
var cell_count = get_cell_count()
	
var current_part:Node2D

static func get_cell_size():
	return 80

static func get_cell_count():
	return Vector2(20, 10)

func _get_minimum_size():
	return cell_count*cell_size

func _draw():
	var offset = line_thickness/2
	for i in cell_count.x+1:
		draw_line(Vector2(i*cell_size, -offset), Vector2(i*cell_size, cell_count.y*cell_size+offset), modulate, line_thickness, true)
	for i in cell_count.y+1:
		draw_line(Vector2(-offset, i*cell_size), Vector2(cell_count.x*cell_size+offset, i*cell_size), modulate, line_thickness, true)
	
func _ready():
	Physics2DServer.set_active(false)
	car.restore_from_file()
	bottom_bar.connect('part_changed', remove_button, 'set', ['pressed', false])

func _input(ev:InputEvent):
	if ev.is_action('remove_all'):
		if ev.pressed:
			for c in car.get_children():
				c.free()
#			var dialog = ConfirmationDialog.new()
#			add_child(dialog)
#			dialog.get_close_button().hide()
#			dialog.connect('popup_hide', dialog, 'queue_free')
#			dialog.window_title = ''
#			dialog.dialog_text = 'remove all ?'.to_upper()
#			dialog.get_label().align = Label.ALIGN_CENTER
#			dialog.get_ok().text = 'YES'
#			dialog.get_cancel().text = 'NO'
#			dialog.connect('confirmed', self, 'remove_all')
#			dialog.popup_centered()
		return
	
	if ev.is_action('drive'):
		car.save_to_file()
		get_tree().change_scene('res://scenes/drive.tscn')
		return
	
	var cell = Vector2.ZERO
	if 'position' in ev:
		if not get_global_rect().has_point(ev.position):
			return
		cell = (make_input_local(ev).position / cell_size).snapped(Vector2.ONE)
	
	if ev is InputEventScreenTouch:
		if ev.pressed:
			if remove_button.pressed:
				var parts = []
				for c in car.get_children():
					parts.append(c)
				parts.sort_custom(self, 'z_index_sort')
				for p in parts:
					if p.has_point(ev.position):
						p.free()
						break
				return
			current_part = load('res://scenes/' + bottom_bar.part + '.tscn').instance()
			car.add_child(current_part)
			current_part.on_editor_touch(cell)
		else:
			if current_part != null:
				current_part.on_editor_release(cell)
			current_part = null
	if ev is InputEventScreenDrag:
		if current_part != null:
			current_part.on_editor_drag(cell)

func z_index_sort(a, b):
	return a.z_index > b.z_index
