extends Node2D
class_name Pivot

onready var sprite = $Sprite
onready var pin = $PinJoint2D
var descriptor:Dictionary
var cell_size = Grid.get_cell_size()

func descriptor_update():
	position = descriptor.position * cell_size

func build():
	pass

func has_point(point:Vector2) -> bool:
	return sprite.get_rect().has_point(sprite.to_local(point))

func on_editor_touch(cell:Vector2):
	descriptor = {
		'type': 'pivot',
		'position': cell,
	}
	descriptor_update()

func on_editor_drag(cell:Vector2):
	pass

func on_editor_release(cell:Vector2):
	pass

