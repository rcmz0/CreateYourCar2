extends Sprite
tool

export(float) var sunrise = 6
export(float) var sunset = 18
export(int) var min_height = 0
export(int) var max_height = 1000

func _process(delta):
	var inverse_viewport_transform = get_viewport_transform().affine_inverse()
	var vp_start = inverse_viewport_transform * Vector2.ZERO 
	var vp_end = inverse_viewport_transform * get_viewport().size
	var vp_center = (vp_start + vp_end) / 2
	var vp_size = vp_end - vp_start
	
	var hour = OS.get_time().hour + OS.get_time().minute/60.0
#	var hour = wrapf(OS.get_system_time_msecs() / 300.0, 0, 24)
#	print(hour)
	
	var delta_height = max_height - min_height
	var radius = ((vp_size.x*vp_size.x)/8 + (delta_height*delta_height)/2) / delta_height
	var pivot = Vector2(vp_center.x, min_height+radius)
	
	var alpha = asin(vp_size.x / (radius * 2))
	var sunpos = inverse_lerp(sunrise, sunset, hour)
	var angle = lerp(-alpha, +alpha, sunpos)
	
	position = pivot + Vector2.UP.rotated(angle)*radius
