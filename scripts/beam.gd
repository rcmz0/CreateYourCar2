extends CollisionShape2D
class_name Beam

onready var main_line = $MainLine2D
onready var detail_line = $DetailLine2D
var descriptor:Dictionary

func get_mass():
	return descriptor.a.distance_to(descriptor.b) * 0.05

func descriptor_update():
	var cell_size = Grid.get_cell_size()
	position = (descriptor.a + descriptor.b) / 2 * cell_size
	rotation = Vector2.DOWN.angle_to(descriptor.a - descriptor.b)
	var length = descriptor.a.distance_to(descriptor.b) * cell_size
	if length == 0: length = 0.001
	shape.height = length
	main_line.points[0].y = -length/2
	main_line.points[1].y = +length/2
	detail_line.points[0].y = -length/2
	detail_line.points[1].y = +length/2

func build():
	load('res://scenes/frame.tscn').instance().build(self)

func has_point(point:Vector2) -> bool:
	return Rect2(
			Vector2(-shape.radius, -shape.height/2-shape.radius),
			Vector2(shape.radius*2, shape.height+shape.radius*2)
		).has_point(to_local(point))

func on_editor_touch(cell:Vector2):
	descriptor = {
		'type': 'beam',
		'a': cell,
		'b': cell,
	}
	descriptor_update()

func on_editor_drag(cell:Vector2):
	descriptor.b = cell
	descriptor_update()
	
func on_editor_release(cell:Vector2):
	pass
