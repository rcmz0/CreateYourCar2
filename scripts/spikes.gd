extends Polygon2D
tool

export var noise:OpenSimplexNoise
export var resolution:float = 10
export var height:float = 50

func _process(delta):
	var contour:Polygon2D = get_parent()
	
	var y_min = INF
	var y_max = -INF
	for p in contour.polygon:
		y_min = min(y_min, p.y)
		y_max = max(y_max, p.y)
	
	var _polygon = PoolVector2Array()
	for i in contour.polygon.size()-1:
		var current = contour.polygon[i]
		var next = contour.polygon[i+1]
		var tangent = (next - current).tangent().normalized()
		var length = current.distance_to(next)
		var number = length/resolution
		for j in number:
			var f = j/number
			var p = current.linear_interpolate(next, f)
			var n = (noise.get_noise_2d(p.x, p.y) + 1) / 2
#			print(y_min, y_max, p.y)
			var y = inverse_lerp(y_min, y_max, p.y)
			_polygon.append(p + tangent*n*y*height)
	polygon = _polygon
