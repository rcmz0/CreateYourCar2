extends CollisionShape2D
class_name Motor

onready var sprite = $Sprite
var direction = 0
var descriptor:Dictionary

func get_mass():
	var size = (descriptor.a - descriptor.b).abs() + Vector2.ONE
	return size.x * size.y * 0.1

func descriptor_update():
	var cell_size = Grid.get_cell_size()
	position = (descriptor.a + descriptor.b) / 2 * cell_size
	var extents = ((descriptor.a - descriptor.b).abs() + Vector2.ONE) / 2 * cell_size
	shape.extents = extents
	sprite.scale = extents * 2

func build():
	load('res://scenes/frame.tscn').instance().build(self)

func has_point(point:Vector2) -> bool:
	return sprite.get_rect().has_point(sprite.to_local(point))

func on_editor_touch(cell:Vector2):
	descriptor = {
		'type': 'motor',
		'a': cell,
		'b': cell,
	}
	descriptor_update()

func on_editor_drag(cell:Vector2):
	descriptor.b = cell
	descriptor_update()
	
func on_editor_release(cell:Vector2):
	pass

func _process(delta):
	direction = int(Input.is_action_pressed('gas')) - int(Input.is_action_pressed('brake'))
