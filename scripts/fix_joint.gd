extends Node2D
class_name FixJoint2D
tool

export(NodePath) var node_a setget set_node_a
export(NodePath) var node_b setget set_node_b
export(float, 0, 16) var softness setget set_softness

func set_node_a(value):
	node_a = value
	for c in get_children():
		c.node_a = '../' + value

func set_node_b(value):
	node_b = value
	for c in get_children():
		c.node_b = '../' + value

func set_softness(value):
	softness = value
	for c in get_children():
		c.softness = value
