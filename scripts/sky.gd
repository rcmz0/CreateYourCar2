extends Polygon2D
tool

func _process(delta):
	var inverse_viewport_transform = get_viewport_transform().affine_inverse()
	var vp_start = inverse_viewport_transform * Vector2.ZERO 
	var vp_end = inverse_viewport_transform * get_viewport().size
	polygon = PoolVector2Array([
		vp_start,
		Vector2(vp_start.x, vp_end.y), 
		vp_end,
		Vector2(vp_end.x, vp_start.y),
	])
