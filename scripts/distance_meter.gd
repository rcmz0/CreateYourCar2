extends Label

export(NodePath) var target_path
onready var target:Node2D = get_node(target_path)

func _process(delta):
	text = round(target.global_position.x / 300) as String
