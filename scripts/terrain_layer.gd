extends Node2D
tool

var points:PoolVector2Array setget set_points
var core_color:Color setget set_core_color
var surface_color:Color setget set_surface_color

func set_points(points):
	if points == null or len(points) == 0:
		return
	
	var x_min = points[0].x
	var x_max = points[-1].x
	
	$Transition.points = points
	$Surface.points = points
	
	$Surface.material.set_shader_param('start', x_min)
	$Surface.material.set_shader_param('length', x_max-x_min)
	
	var polygon = PoolVector2Array()
	polygon.append_array(points)
	polygon.append(Vector2(x_max, 1e6))
	polygon.append(Vector2(x_min, 1e6))
	$Core.polygon = polygon

func set_core_color(value:Color):
	$Core.color = value
	$Transition.material.set_shader_param('core', value)

func set_surface_color(value:Color):
	$Surface.default_color = value
	$Transition.material.set_shader_param('surface', value)
