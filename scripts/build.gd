extends Node

var current_part:Node2D
var touch_position:Vector2

onready var car = $Car
onready var grid = $CenterContainer/Grid
onready var bottom_bar = $MarginContainer/Control/BottomBar

func _ready():
	Physics2DServer.set_active(false)
	car.restore_from_file()

func _input(ev:InputEvent):
	if ev.is_action('remove_all'):
		if ev.pressed:
			for c in car.get_children():
				c.free()
#			var dialog = ConfirmationDialog.new()
#			add_child(dialog)
#			dialog.get_close_button().hide()
#			dialog.connect('popup_hide', dialog, 'queue_free')
#			dialog.window_title = ''
#			dialog.dialog_text = 'remove all ?'.to_upper()
#			dialog.get_label().align = Label.ALIGN_CENTER
#			dialog.get_ok().text = 'YES'
#			dialog.get_cancel().text = 'NO'
#			dialog.connect('confirmed', self, 'remove_all')
#			dialog.popup_centered()
		return
	
	var cell = Vector2.ZERO
	if 'position' in ev:
		if grid.get_global_rect().has_point(ev.position):
			cell = (grid.make_input_local(ev).position / grid.cell_size).snapped(Vector2.ONE)
		else:
			return
#		ev.position = ev.position.snapped(Vector2.ONE * grid_size)
	
#	if current_tool == 'remove':
#		if ev is InputEventScreenTouch:
#			if ev.pressed:
#				for c in car.get_children():
#					if c.has_point(ev.position):
#						car.remove_child(c)
#						break
	
	if ev is InputEventScreenTouch:
		if ev.pressed:
			touch_position = cell
			current_part = load('res://scenes/' + bottom_bar.part + '.tscn').instance()
			car.add_child(current_part)
			if current_part.on_editor_touch(cell) == true:
				car.remove_child(current_part)
				current_part = null
		else:
			if current_part:
				if current_part.on_editor_release(cell) == true:
					car.remove_child(current_part)
			current_part = null
	if ev is InputEventScreenDrag:
		if current_part:
			current_part.on_editor_drag(touch_position, cell)

func on_drive():
	car.save_to_file()
	get_tree().change_scene('res://scenes/drive.tscn')
