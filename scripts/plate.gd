extends CollisionShape2D
class_name Plate

onready var sprite = $Sprite
var descriptor:Dictionary

func descriptor_update():
	position = descriptor.center
	shape.extents = descriptor.extents
	sprite.scale = descriptor.extents * 2

func build():
	var car = get_parent()
	var touching_frames = []
	for f in car.get_children():
		if f is Frame:
			for p in f.get_children():
				if 'descriptor' in p and p.descriptor.type == 'plate':
					if get_rect().intersects(p.get_rect()):
						touching_frames.append(f)
						break
	var frame = load('res://scenes/frame.tscn').instance()
	car.add_child(frame)
	car.remove_child(self)
	frame.add_child(self)
	for f in touching_frames:
		for p in f.get_children():
			if 'descriptor' in p and p.descriptor.type == 'plate':
				var g = p.global_position
				f.remove_child(p)
				frame.add_child(p)
				p.global_position = g
		car.remove_child(f)
	
	var plates = []
	for p in frame.get_children():
		if 'descriptor' in p and p.descriptor.type == 'plate':
			plates.append(p)
	var center_of_gravity = Vector2.ZERO
	for p in plates:
		center_of_gravity += p.global_position
	center_of_gravity /= len(plates)
	frame.global_position += center_of_gravity
	for p in plates:
		p.global_position -= center_of_gravity

func has_point(point:Vector2) -> bool:
	return get_rect().has_point(point)

func get_rect() -> Rect2:
	return Rect2(global_position - shape.extents, shape.extents * 2)

func on_editor_touch(touch_position:Vector2):
	descriptor = {
		'type': 'plate',
		'center': touch_position,
		'extents': Vector2.ZERO,
	}
	descriptor_update()

func on_editor_drag(touch_position:Vector2, drag_position:Vector2):
	descriptor.center = (touch_position + drag_position) / 2
	descriptor.extents = ((drag_position - touch_position) / 2).abs()
	descriptor_update()
	
func on_editor_release(release_position:Vector2):
	pass
