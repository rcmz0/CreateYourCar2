extends RigidBody2D
class_name Frame

func has_point(point:Vector2):
	for c in get_children():
		if (c is Beam or c is PivotBeam or c is Motor) and c.has_point(point):
			return true
	return false

func build(base:CollisionShape2D):
	var car = base.get_parent()
	car.add_child(self)
	car.remove_child(base)
	add_child(base)
	
	for f in car.get_children():
		if f is get_script() and f != self:
			for c in f.get_children():
				if base.shape.collide(base.global_transform, c.shape, c.global_transform):
					for cc in f.get_children():
						var g = cc.global_transform
						f.remove_child(cc)
						add_child(cc)
						cc.global_transform = g
					f.free()
					break
	
	var m = 0
	var cg = Vector2.ZERO
	for c in get_children():
		m += c.get_mass()
		cg += c.global_position * c.get_mass()
	mass = m
	cg /= m
	var cg_offset = cg - global_position
	global_position += cg_offset
	for c in get_children():
		c.global_position -= cg_offset
#
#func _draw():
#	draw_circle(Vector2.ZERO, 100, Color.deeppink)
